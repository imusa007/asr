.. _API reference:

=============
API reference
=============

.. contents::
   :local:

.. currentmodule:: asr

Recipes
=======
.. autosummary::
   :template: autosummary/mytemplate.rst
   :toctree: generated

   bader
   bandstructure
   berry
   borncharges
   bse
   convex_hull
   defectformation
   deformationpotentials
   dos
   emasses
   fere
   fermisurface
   formalpolarization
   gs
   gw
   hse
   infraredpolarizability
   magnetic_anisotropy
   pdos
   phonons
   phonopy
   piezoelectrictensor
   plasmafrequency
   polarizability
   projected_bandstructure
   push
   raman
   relax
   stiffness
   structureinfo
   workflow

Setup sub-package
=================
.. currentmodule:: asr.setup

.. autosummary::
   :template: autosummary/mytemplate.rst
   :toctree: generated

   decorate
   defects
   magnetize
   materials
   params
   scanparams
   strains
   symmetrize

Database sub-package
====================
.. currentmodule:: asr.database

.. autosummary::
   :template: autosummary/mytemplate.rst
   :toctree: generated

   app
   clonetree
   fromtree
   material_fingerprint
   merge
   totree


Core sub-package
================
.. currentmodule:: asr

.. autosummary::
   :template: autosummary/mytemplate.rst
   :toctree: generated

   core
   core.cli
   core.material

.. _api test:

Test sub-package
================

.. currentmodule:: asr

.. autosummary::
   :template: autosummary/mytemplate.rst
   :toctree: generated

   test
   test.conftest
   test.fixtures
   test.materials
   test.mocks
   test.mocks.gpaw
   test.mocks.gpaw.GPAW
