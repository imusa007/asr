Installation
============
Install ASE, ASR and GPAW from PyPI::

  $ pip install asr
  $ pip install gpaw

Additionally, you might also want

* myqueue

if you want to run jobs on a computer-cluster.

Finally, test the package with::

  $ asr test
