from .magnetism import is_magnetic, magnetic_atoms, mag_elements  # noqa
from .calculator_utils import fermi_level, get_eigenvalues  # noqa
